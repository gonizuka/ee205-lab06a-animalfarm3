///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Gavin Onizuka <gonizuka@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   23_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene( string newTag, enum Color newColor, enum Gender newGender ) {
   gender = newGender;         
   species = "Branta sandvicensis";    
   featherColor = newColor;       
   isMigratory = true;       
   tag = newTag;             
}


const string Nene::speak() {
   return string( "Nay, nay" );
}


/// Print our Nene and name first... then print whatever information Mammal holds.
void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << tag << "]" << endl;
   Bird::printInfo();
}

} 

