#include "animalfactory.hpp"

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

namespace animalfarm{ 

   Animal* AnimalFactory::getRandomAnimal(){
      Animal* newAnimal = NULL;
      int i = rand() % 6;

      switch(i){
         case 0: newAnimal = new Cat   (Cat::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
         case 1: newAnimal = new Dog   (Dog::getRandomName(), Animal::getRandomColor(),  Animal::getRandomGender()); break;
         case 2: newAnimal = new Nunu  ( Animal::getRandomBool(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
         case 3: newAnimal = new Aku   ( Animal::getRandomWeight(2.2,12.4), Animal::getRandomColor(),Animal::getRandomGender()); break;
         case 4: newAnimal = new Palila( Animal::getRandomName(), YELLOW, Animal::getRandomGender());   break;
         case 5: newAnimal = new Nene  ( Animal::getRandomName(), BROWN, Animal::getRandomGender());     break;
   }

   return newAnimal;
}
}

