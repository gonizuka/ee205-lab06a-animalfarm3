///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file fish.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Gavin Onizuka <gonizuka@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   23_02_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "animal.hpp"

namespace animalfarm {

class Fish : public Animal {
public:
   enum Color scaleColor;
   float        favoriteTemp;

   void printInfo();
   virtual const string speak();
};

}

